#pragma once

#include <pico/types.h>
#include <stdint.h>


void keyboard_init();
// 6 kyes + 8 modifiers + 1 extend + 1 extra
int keyboard_get(int pressed_keys[16]);