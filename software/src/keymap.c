#include <pico/types.h>
#include <stdint.h>
#include <tusb.h>

const uint8_t modifiers[] = {
             0              ,             0              ,             0              ,KEYBOARD_MODIFIER_LEFTSHIFT , KEYBOARD_MODIFIER_LEFTCTRL ,
             0              ,             0              ,             0              ,             0              , KEYBOARD_MODIFIER_LEFTALT  ,
             0              ,             0              ,             0              ,             0              , KEYBOARD_MODIFIER_LEFTGUI  ,
             0              ,             0              ,             0              ,             0              ,             0              ,
             0              ,             0              ,             0              ,             0              ,             0              ,
             0              ,             0              ,             0              ,             0              ,             0              ,
             0              ,             0              ,             0              ,             0              ,             0              ,
             0              ,             0              ,             0              ,             0              ,             0              ,
             0              ,             0              ,             0              ,             0              ,             0              ,
             0              ,             0              ,             0              ,             0              ,             0              ,
             0              ,             0              ,             0              ,             0              , KEYBOARD_MODIFIER_RIGHTALT ,
             0              ,             0              ,             0              ,             0              , KEYBOARD_MODIFIER_RIGHTGUI ,
             0              ,             0              ,             0              ,             0              ,KEYBOARD_MODIFIER_RIGHTCTRL ,
             0              ,             0              ,             0              ,KEYBOARD_MODIFIER_RIGHTSHIFT,             0              ,
};


const uint8_t layers[] = {
       HID_KEY_GRAVE        ,        HID_KEY_TAB         ,        HID_KEY_NONE        ,     HID_KEY_SHIFT_LEFT     ,    HID_KEY_CONTROL_LEFT    ,
         HID_KEY_1          ,         HID_KEY_Q          ,         HID_KEY_A          ,         HID_KEY_X          ,      HID_KEY_ALT_LEFT      ,
         HID_KEY_2          ,         HID_KEY_W          ,         HID_KEY_R          ,         HID_KEY_C          ,      HID_KEY_GUI_LEFT      ,
         HID_KEY_3          ,         HID_KEY_F          ,         HID_KEY_S          ,         HID_KEY_D          ,        HID_KEY_NONE        ,
         HID_KEY_4          ,         HID_KEY_P          ,         HID_KEY_T          ,         HID_KEY_V          ,        HID_KEY_NONE        ,
         HID_KEY_5          ,         HID_KEY_B          ,         HID_KEY_G          ,        HID_KEY_NONE        ,        HID_KEY_NONE        ,
         HID_KEY_6          ,    HID_KEY_BRACKET_LEFT    ,        HID_KEY_NONE        ,         HID_KEY_Z          ,       HID_KEY_SPACE        ,
       HID_KEY_EQUAL        ,         HID_KEY_J          ,   HID_KEY_BRACKET_RIGHT    ,        HID_KEY_NONE        ,        HID_KEY_NONE        ,
         HID_KEY_7          ,         HID_KEY_L          ,         HID_KEY_M          ,       HID_KEY_SLASH        ,        HID_KEY_NONE        ,
         HID_KEY_8          ,         HID_KEY_U          ,         HID_KEY_N          ,         HID_KEY_K          ,        HID_KEY_NONE        ,
         HID_KEY_9          ,         HID_KEY_Y          ,         HID_KEY_E          ,         HID_KEY_H          ,     HID_KEY_ALT_RIGHT      ,
         HID_KEY_0          ,     HID_KEY_SEMICOLON      ,         HID_KEY_I          ,       HID_KEY_COMMA        ,     HID_KEY_GUI_RIGHT      ,
       HID_KEY_MINUS        ,     HID_KEY_APOSTROPHE     ,         HID_KEY_O          ,       HID_KEY_PERIOD       ,   HID_KEY_CONTROL_RIGHT    ,
     HID_KEY_BACKSPACE      ,     HID_KEY_BACKSLASH      ,       HID_KEY_ENTER        ,    HID_KEY_SHIFT_RIGHT     ,     HID_KEY_CAPS_LOCK      ,



       HID_KEY_ESCAPE       ,        HID_KEY_TAB         ,        HID_KEY_NONE        ,     HID_KEY_SHIFT_LEFT     ,    HID_KEY_CONTROL_LEFT    ,
         HID_KEY_F1         ,        HID_KEY_NONE        ,        HID_KEY_NONE        ,    HID_KEY_BRACKET_LEFT    ,      HID_KEY_ALT_LEFT      ,
         HID_KEY_F2         ,        HID_KEY_HOME        ,     HID_KEY_ARROW_LEFT     ,   HID_KEY_BRACKET_RIGHT    ,      HID_KEY_GUI_LEFT      ,
         HID_KEY_F3         ,      HID_KEY_ARROW_UP      ,     HID_KEY_ARROW_DOWN     ,         HID_KEY_9          ,        HID_KEY_NONE        ,
         HID_KEY_F4         ,        HID_KEY_END         ,    HID_KEY_ARROW_RIGHT     ,         HID_KEY_0          ,        HID_KEY_NONE        ,
         HID_KEY_F5         ,      HID_KEY_PAGE_UP       ,     HID_KEY_PAGE_DOWN      ,        HID_KEY_NONE        ,       HID_KEY_SPACE        ,
         HID_KEY_F6         ,        HID_KEY_NONE        ,        HID_KEY_NONE        ,        HID_KEY_NONE        ,        HID_KEY_NONE        ,
         HID_KEY_F7         ,       HID_KEY_EQUAL        ,        HID_KEY_NONE        ,        HID_KEY_NONE        ,        HID_KEY_NONE        ,
         HID_KEY_F8         ,         HID_KEY_7         ,  HID_KEY_KEYPAD_MULTIPLY   ,        HID_KEY_NONE        ,        HID_KEY_NONE        ,
         HID_KEY_F9         ,         HID_KEY_8         ,         HID_KEY_4         ,         HID_KEY_0         ,        HID_KEY_NONE        ,
        HID_KEY_F10         ,        HID_KEY_9         ,         HID_KEY_5         ,         HID_KEY_1         ,     HID_KEY_ALT_RIGHT      ,
        HID_KEY_F11         ,  HID_KEY_KEYPAD_SUBTRACT   ,         HID_KEY_6         ,         HID_KEY_2         ,     HID_KEY_GUI_RIGHT      ,
        HID_KEY_F12         ,   HID_KEY_KEYPAD_DIVIDE    ,     HID_KEY_KEYPAD_ADD     ,         HID_KEY_3         ,   HID_KEY_CONTROL_RIGHT    ,
       HID_KEY_DELETE       ,        HID_KEY_NONE        ,       HID_KEY_ESCAPE       ,    HID_KEY_SHIFT_RIGHT     ,      HID_KEY_NUM_LOCK      ,
};

void keymap_map(int pressed_keys_count, int pressed_keys[16], uint8_t keycodes[6], uint8_t* modifier)
{
  int layer = 0;
  for (int i = 0; i < pressed_keys_count; ++i) {
    if (pressed_keys[i] == 2) {
      layer = 70;
      break;
    }
  }

  for (int i = 0; i < 6; ++i) {
    keycodes[i] = 0;
  }

  *modifier = 0;

  int key = 0;
  for (int i = 0; i < pressed_keys_count; ++i) {
    *modifier |= modifiers[pressed_keys[i]];
    if (key < 6) {
      keycodes[key++] = layers[layer + pressed_keys[i]];
    }
  }
}