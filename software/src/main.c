#include <pico/stdlib.h>
#include <hardware/gpio.h>
#include <hardware/pio.h>
#include <hardware/clocks.h>
#include <bsp/board.h>
#include <device/usbd.h>
#include <tusb.h>

#include "tusb_config.h"

#include "keyboard.h"
#include "keymap.h"
#include "pico/time.h"
#include "usb_descriptors.h"


void hid_task(void);

int main(void) {
	board_init();

	keyboard_init();

  	tud_init(BOARD_TUD_RHPORT);

	while (1) {
		tud_task();
		hid_task();
	}

	return 0;
}

int pressed_keys[16];
uint8_t keycodes[6];
uint8_t modifier;

void hid_task(void) {
	// Poll every 10ms
	const uint32_t interval_ms = 10;
	static uint32_t start_ms = 0;
	uint8_t const report_id = 0;

	if ( board_millis() - start_ms < interval_ms) return; // not enough time
	start_ms += interval_ms;

	//uint32_t const btn = board_button_read();
	int pressed_keys_count = keyboard_get(pressed_keys);

	// Remote wakeup
	if ( tud_suspended() && pressed_keys_count )
	{
		// Wake up host if we are in suspend mode
		// and REMOTE_WAKEUP feature is enabled by host
		tud_remote_wakeup();
	}else
	{
		if ( tud_hid_n_ready(ITF_NUM_KEYBOARD) )
		{
			static bool was_pressed = false;

			if (pressed_keys_count) {
				keymap_map(pressed_keys_count, pressed_keys, keycodes, &modifier);
        		tud_hid_n_keyboard_report(ITF_NUM_KEYBOARD, report_id, modifier, keycodes);

				was_pressed = true;
			} else {
        		if (was_pressed) tud_hid_n_keyboard_report(ITF_NUM_KEYBOARD, report_id, 0, NULL);
				was_pressed = false;
			}
		}
	}
}

// Invoked when received GET_REPORT control request
// Application must fill buffer report's content and return its length.
// Return zero will cause the stack to STALL request
uint16_t tud_hid_get_report_cb(uint8_t instance, uint8_t report_id, hid_report_type_t report_type, uint8_t* buffer, uint16_t reqlen) {
	// TODO not Implemented
	(void) instance;
	(void) report_id;
	(void) report_type;
	(void) buffer;
	(void) reqlen;

	return 0;
}

// Invoked when received SET_REPORT control request or
// received data on OUT endpoint ( Report ID = 0, Type = 0 )
void tud_hid_set_report_cb(uint8_t instance, uint8_t report_id, hid_report_type_t report_type, uint8_t const* buffer, uint16_t bufsize) {
	(void)instance;
	(void)report_id;
	(void)report_type;
	(void)buffer;
	(void)bufsize;
}