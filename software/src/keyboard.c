#include "keyboard.h"
#include "keyboard.pio.h"

// Slow down the PIO by CLKDIV to allow for voltages to settle between set/get
const float CLKDIV = 64;

const uint IN_PINS_BASE = 0;
const uint IN_PINS_COUNT = keyboard_IN_PINS_COUNT;
const uint OUT_PINS_BASE = IN_PINS_COUNT;
const uint OUT_PINS_COUNT = 14;
const uint PINS_COUNT = IN_PINS_COUNT + OUT_PINS_COUNT;

const PIO pio = pio0;
uint sm;

void keyboard_init() {
	uint offset = pio_add_program(pio, (const pio_program_t *)&keyboard_program);
	sm = pio_claim_unused_sm(pio, true);

	pio_sm_config c = keyboard_program_get_default_config(offset);

	sm_config_set_out_pins(&c, OUT_PINS_BASE, OUT_PINS_COUNT);
	sm_config_set_in_pins(&c, IN_PINS_BASE);

	for (int i = 0; i < IN_PINS_COUNT; ++i) {
		pio_gpio_init(pio, IN_PINS_BASE + i);
	}

	for (int i = 0; i < OUT_PINS_COUNT; ++i) {
		pio_gpio_init(pio, OUT_PINS_BASE + i);
	}

	pio_sm_set_consecutive_pindirs(pio, sm, OUT_PINS_BASE, OUT_PINS_COUNT, true);
	sm_config_set_out_shift(&c, true, true, OUT_PINS_COUNT);

	pio_sm_set_consecutive_pindirs(pio, sm, IN_PINS_BASE, OUT_PINS_BASE, false);
	sm_config_set_in_shift(&c, false, true, OUT_PINS_BASE);

	sm_config_set_clkdiv(&c, CLKDIV);

	pio_sm_init(pio, sm, offset, &c);
	pio_sm_set_enabled(pio, sm, true);
}

int keyboard_get(int pressed_keys[16])
{
	int pressed_keys_count = 0;

	pio_sm_put_blocking(pio, sm, 1<<OUT_PINS_COUNT);
	for (int i = 0; i < OUT_PINS_COUNT; ++i) {
		uint8_t bits = pio_sm_get_blocking(pio, sm);
		for (int j = 0; j < IN_PINS_COUNT; ++j) {
			uint8_t mask = 1u << j;
			if (pressed_keys_count < 16 && bits & mask) {
				pressed_keys[pressed_keys_count++] = i * IN_PINS_COUNT + j;
			}
		}
	}

	return pressed_keys_count;
}
