# Simple Modular Keyboard

The goal of this project was to design a keyboard that would be simple to make and redesign.

![Finished keyboard](finished_keyboard.jpg "Finished keyboard")


## Hardware

The key-matrix is separated from any electronics - just a d-sub pinout is provided. It was designed for Cherry MX plate mounted switches with integrated rectifier diodes. They are arranged in the standard ANSI layout. For the controller the RaspberryPi Pico was chosen. More accessories can be easily connected to it as needed.

![Whole keyboard](whole_keyboard.jpg "Whole keyboard")

## Software

Code is based on the TinyUSB examples. Provided source code implements a Colemak-DH wide ANSI mapping with a custom Extend layer (visualisation made with [keyboard-layout-editor](keyboard-layout-editor.com)):

![Keyboard layout](keyboard-layout.png "Keyboard layout")
